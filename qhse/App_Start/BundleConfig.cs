﻿using System.Web;
using System.Web.Optimization;

namespace qhse
{
    public class BundleConfig
    {
        // Pour plus d'informations sur le regroupement, visitez https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            /*var css = new StyleBundle("~/bundles/css"){
                Orderer = new CustomBundleOrderer()
            };
            css.Include(
                "~/bower_components/bootstrap/dist/css/bootstrap.css",
                "~/bower_components/font-awesome/css/font-awesome.css",
                "~/bower_components/Ionicons/css/ionicons.css",
                "~/dist/css/AdminLTE.css",
                "~/dist/css/skins/_all-skins.css",
                "~/bower_components/morris.js/morris.css",
                "~/bower_components/jvectormap/jquery-jvectormap.css",
                "~/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css",
                "~/bower_components/bootstrap-daterangepicker/daterangepicker.css",
                "~/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css");
            bundles.Add(css);*/

            /*bundles.Add(new ScriptBundle("~/bundles/html5shiv", "https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/respond", "https://oss.maxcdn.com/respond/1.4.2/respond.min.js"));*/

            /*bundles.Add(new StyleBundle("~/bundles/googleapis", "https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"));

            var js = new StyleBundle("~/bundles/js"){
                Orderer = new CustomBundleOrderer()
            };
            js.Include(
                "~/bower_components/jquery/dist/jquery.js",
                "~/bower_components/jquery-ui/jquery-ui.js",
                "~/bower_components/bootstrap/dist/js/bootstrap.js",
                "~/bower_components/raphael/raphael.js",
                "~/bower_components/morris.js/morris.js",
                "~/bower_components/jquery-sparkline/dist/jquery.sparkline.js",
                "~/plugins/jvectormap/jquery-jvectormap-1.2.2.js",
                "~/plugins/jvectormap/jquery-jvectormap-world-mill-en.js",
                "~/bower_components/jquery-knob/dist/jquery.knob.js",
                "~/bower_components/moment/min/moment.js",
                "~/bower_components/bootstrap-daterangepicker/daterangepicker.js",
                "~/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js",
                "~/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js",
                "~/bower_components/jquery-slimscroll/jquery.slimscroll.js",
                "~/bower_components/fastclick/lib/fastclick.js",
                "~/dist/js/adminlte.js",
                "~/dist/js/pages/dashboard.js",
                "~/dist/js/demo.js");
            bundles.Add(js);

            //Set EnableOptimizations to false for debugging. For more infromation,
            // visite http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = true;
            bundles.UseCdn = true;*/
        }
    }
}
