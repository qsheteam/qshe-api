﻿using qhse.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;

namespace qhse.Core
{
    public class ContentService
    {
        private static RavinalaEntities db = new RavinalaEntities();
        public static Content ReadToObject(string json)
        {
            var deserializedUser = new Content();
            var ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
            var ser = new DataContractJsonSerializer(deserializedUser.GetType());
            deserializedUser = ser.ReadObject(ms) as Content;
            ms.Close();
            return deserializedUser;
        }
        public static int insert(Content content)
        {
            db.Content.Add(content);
            return db.SaveChanges();
        }
        public static int insertionMultiple(List<Content> content)
        {
            int finInsertion = 0;
            try
            {
                foreach(Content cntt in content)
                {
                    insert(cntt);
                }
                finInsertion = 1;
            }catch(Exception ex)
            {
                finInsertion = 0;
                _ = ex.Message;
            }
            return finInsertion;
        } 
        public static int atoi(string mot)
        {
            int AtoI = 0;
            Int32.TryParse(mot, out AtoI);
            return AtoI;
        }
        
    }
}