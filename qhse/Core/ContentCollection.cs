﻿using qhse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace qhse.Core
{
    public class ContentCollection
    {
        public IEnumerable<Content> Contents { get; set; }
    }

}