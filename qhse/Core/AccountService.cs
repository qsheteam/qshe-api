﻿using qhse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace qhse.Core
{
    public class AccountService
    {
        private static RavinalaEntities db = new RavinalaEntities();

        public static User FindUserByUsername(string username)
        {
            var user = db.User.Where(u => u.USERNAME == username).FirstOrDefault<User>();
            return user;
        }

        public static User Signin(string username, string password)
        {
            var u = FindUserByUsername(username);
            if(u == null)
            {
                return null;
            }
            if (u.PASSWORD != password)
            {
                return null;
            }
            return u;
        }
    }
}