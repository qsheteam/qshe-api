﻿using qhse.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace qhse.Core
{
    public class SectionService
    {
        //obligatoire connect base
        private static RavinalaEntities db = new RavinalaEntities();

        public static List<Section> getAllSection()
        {
            var reponse = new List<Section>();
            reponse = db.Section.SqlQuery("Select * from Section").ToList<Section>();
            return reponse;
        }
        public static SectionChild getSectionChild(decimal idsection, List<Section> data)
        {
            SectionChild reponse = new SectionChild();
            foreach(var section in data)
            {
                if(section.ID_SECTION == idsection)
                {
                    reponse.CopyInto(section);
                }
                if(section.SEC_ID_SECTION == idsection)
                {
                    SectionChild temp = getSectionChild(section.ID_SECTION, data);
                    reponse.Enfants.Add(temp);
                }
            }

            
            return reponse;
        }
        public static List<SectionChild> listArranger(List<Section> listeSection)
        {
            List<SectionChild> reponseFinal = new List<SectionChild>();
            foreach( var section in listeSection)
            {
                if(section.SEC_ID_SECTION == null)
                {
                    SectionChild temp = getSectionChild(section.ID_SECTION, listeSection);
                    reponseFinal.Add(temp);
                }
            }
            return reponseFinal;
        }

        public static void insertTree(List<SectionChild> sections)
        {
            foreach(var section in sections)
            {
                save(null, section);
            }
        }

        private static void save(Section parent, SectionChild section)
        {
            var sec = section.ToSection();
            sec.ICONE = sec.NOM_SECTION;
            if(parent == null)
            {
                sec.SEC_ID_SECTION = null;
            } else
            {
                sec.SEC_ID_SECTION = parent.ID_SECTION;
            }
            db.Section.Add(sec);
            try
            {
                db.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }

            foreach (var tmp in section.Enfants)
            {
                save(sec, tmp);
            }
        }
    }
}