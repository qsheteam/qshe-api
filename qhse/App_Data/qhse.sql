/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     14/11/2019 11:21:36                          */
/*==============================================================*/


if exists (select 1
            from  sysindexes
           where  id    = object_id('Content')
            and   name  = 'RUBRIQUE_CONTENT_REL_FK'
            and   indid > 0
            and   indid < 255)
   drop index Content.RUBRIQUE_CONTENT_REL_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Content')
            and   name  = 'SECTION_CONTENT_REL_FK'
            and   indid > 0
            and   indid < 255)
   drop index Content.SECTION_CONTENT_REL_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Content')
            and   name  = 'USER_CONTENT_REL_FK'
            and   indid > 0
            and   indid < 255)
   drop index Content.USER_CONTENT_REL_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Content')
            and   type = 'U')
   drop table Content
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Rubrique')
            and   name  = 'RUBRIQU_SECTION_REL_FK'
            and   indid > 0
            and   indid < 255)
   drop index Rubrique.RUBRIQU_SECTION_REL_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Rubrique')
            and   type = 'U')
   drop table Rubrique
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Section')
            and   name  = 'SECTION_PARENT_REL_FK'
            and   indid > 0
            and   indid < 255)
   drop index Section.SECTION_PARENT_REL_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Section')
            and   type = 'U')
   drop table Section
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Status')
            and   type = 'U')
   drop table Status
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('"User"')
            and   name  = 'STATUS_USER_REL_FK'
            and   indid > 0
            and   indid < 255)
   drop index "User".STATUS_USER_REL_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('"User"')
            and   type = 'U')
   drop table "User"
go

/*==============================================================*/
/* Table: Content                                               */
/*==============================================================*/
create table Content (
   ID_CONTENT           numeric              identity,
   ID_USER              numeric              null,
   ID_RUBRIQUE          numeric              null,
   ID_SECTION           numeric              null,
   NOM_CONTENT          text                 null,
   HEURE                datetime             not null,
   DATE                 datetime             not null,
   ETAT                 bit                  not null,
   PROPRETE             bit                  not null,
   IMAGE                varchar(60)          null,
   constraint PK_CONTENT primary key nonclustered (ID_CONTENT)
)
go

/*==============================================================*/
/* Index: USER_CONTENT_REL_FK                                   */
/*==============================================================*/
create index USER_CONTENT_REL_FK on Content (
ID_USER ASC
)
go

/*==============================================================*/
/* Index: SECTION_CONTENT_REL_FK                                */
/*==============================================================*/
create index SECTION_CONTENT_REL_FK on Content (
ID_SECTION ASC
)
go

/*==============================================================*/
/* Index: RUBRIQUE_CONTENT_REL_FK                               */
/*==============================================================*/
create index RUBRIQUE_CONTENT_REL_FK on Content (
ID_RUBRIQUE ASC
)
go

/*==============================================================*/
/* Table: Rubrique                                              */
/*==============================================================*/
create table Rubrique (
   ID_RUBRIQUE          numeric              identity,
   ID_SECTION           numeric              null,
   NOM_RUBRIQUE         varchar(45)          null,
   constraint PK_RUBRIQUE primary key nonclustered (ID_RUBRIQUE)
)
go

/*==============================================================*/
/* Index: RUBRIQU_SECTION_REL_FK                                */
/*==============================================================*/
create index RUBRIQU_SECTION_REL_FK on Rubrique (
ID_SECTION ASC
)
go

/*==============================================================*/
/* Table: Section                                               */
/*==============================================================*/
create table Section (
   ID_SECTION           numeric              identity,
   IDPARENT             numeric              null,
   NOM_SECTION          varchar(45)          not null,
   ICONE                varchar(45)          not null,
   AS_TABS              bit                  not null,
   constraint PK_SECTION primary key nonclustered (ID_SECTION)
)
go

/*==============================================================*/
/* Index: SECTION_PARENT_REL_FK                                 */
/*==============================================================*/
create index SECTION_PARENT_REL_FK on Section (
IDPARENT ASC
)
go

/*==============================================================*/
/* Table: Status                                                */
/*==============================================================*/
create table Status (
   ID_STATUS            numeric              identity,
   NOM_STATUS           varchar(45)          null,
   constraint PK_STATUS primary key nonclustered (ID_STATUS)
)
go

/*==============================================================*/
/* Table: "User"                                                */
/*==============================================================*/
create table "User" (
   ID_USER              numeric              identity,
   ID_STATUS            numeric              null,
   USERNAME             varchar(45)          null,
   PASSWORD             varchar(45)          null,
   constraint PK_USER primary key nonclustered (ID_USER)
)
go

/*==============================================================*/
/* Index: STATUS_USER_REL_FK                                    */
/*==============================================================*/
create index STATUS_USER_REL_FK on "User" (
ID_STATUS ASC
)
go

