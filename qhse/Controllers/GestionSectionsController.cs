﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using qhse.Models;

namespace qhse.Controllers
{
    public class GestionSectionsController : BaseController
    {
        private readonly RavinalaEntities db = new RavinalaEntities();

        // GET: SectionsAdmin
        public ActionResult Index()
        {
            var section = db.Section.Include(s => s.Section2);
            return View(section.ToList());
        }

        // GET: SectionsAdmin/Create
        public ActionResult Create()
        {
            SelectList list = new SelectList(db.Section.Where(Section => Section.SEC_ID_SECTION == null), "ID_SECTION", "NOM_SECTION");
            ViewBag.SEC_ID_SECTION = list.Prepend(new SelectListItem { Value = null });
            return View();
        }

        // POST: SectionsAdmin/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SEC_ID_SECTION,NOM_SECTION,IconFile,AS_TABS,SEARCH,PARENT_SEC_ID_SECTION")] SectionModele sectionmodele)
        {
            if (ModelState.IsValid && sectionmodele.SEARCH == 0)
            {
                string directoryPath = Server.MapPath("~/dist/img/icon/");
                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }
                string fileName = Path.GetFileName(sectionmodele.IconFile.FileName);
                string extension = Path.GetExtension(fileName);
                string nameWithoutExtension = Path.GetFileNameWithoutExtension(fileName);
                string path = Path.Combine(Server.MapPath("~/dist/img/icon/"), fileName);
                var i = 1;
                while (System.IO.File.Exists(path))
                {
                    fileName = nameWithoutExtension.Trim() + " (" + i + ")" + extension;
                    path = Path.Combine(Server.MapPath("~/dist/img/icon/"), fileName);
                    i++;
                }
                sectionmodele.IconFile.SaveAs(path);
                sectionmodele.ICONE = fileName;
                Section section = new Section
                {
                    SEC_ID_SECTION = sectionmodele.SEC_ID_SECTION,
                    NOM_SECTION = sectionmodele.NOM_SECTION,
                    ICONE = sectionmodele.ICONE,
                    AS_TABS = sectionmodele.AS_TABS
                };
                db.Section.Add(section);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            SelectList list = null;
            if (sectionmodele.SEARCH == 1)
            {
                list = new SelectList(db.Section.Where(Section => Section.SEC_ID_SECTION == sectionmodele.SEC_ID_SECTION), "ID_SECTION", "NOM_SECTION");
                ModelState.Clear();
                sectionmodele.PARENT_SEC_ID_SECTION = sectionmodele.SEC_ID_SECTION;
            }
            else if (sectionmodele.SEARCH == 0)
            {
                list = new SelectList(db.Section.Where(Section => Section.SEC_ID_SECTION == sectionmodele.PARENT_SEC_ID_SECTION), "ID_SECTION", "NOM_SECTION", sectionmodele.SEC_ID_SECTION);
            }
            ViewBag.SEC_ID_SECTION = list.Prepend(new SelectListItem { Value = null });
            return View(sectionmodele);
        }

        // GET: SectionsAdmin/Edit/5
        public ActionResult Edit(decimal id)
        {
            if (id.Equals(null))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Section section = db.Section.Find(id);
            if (section == null)
            {
                return HttpNotFound();
            }
            Section parentSec = db.Section.Where(Section => Section.ID_SECTION == section.SEC_ID_SECTION).FirstOrDefault();
            if (parentSec == null)
            {
                parentSec = new Section { SEC_ID_SECTION = null };
            }
            SectionEditModele sectionmodele = new SectionEditModele
            {
                ID_SECTION = section.ID_SECTION,
                SEC_ID_SECTION = section.SEC_ID_SECTION,
                NOM_SECTION = section.NOM_SECTION,
                ICONE = section.ICONE,
                AS_TABS = section.AS_TABS,
                PARENT_SEC_ID_SECTION = parentSec.SEC_ID_SECTION
            };
            SelectList list = new SelectList(db.Section.Where(Section => Section.SEC_ID_SECTION == sectionmodele.PARENT_SEC_ID_SECTION), "ID_SECTION", "NOM_SECTION", section.SEC_ID_SECTION);
            ViewBag.SEC_ID_SECTION = list.Prepend(new SelectListItem { Value = null });
            return View(sectionmodele);
        }

        // POST: SectionsAdmin/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_SECTION,SEC_ID_SECTION,NOM_SECTION,IconFile,ICONE,AS_TABS,SEARCH,PARENT_SEC_ID_SECTION")] SectionEditModele sectionmodele)
        {
            if (ModelState.IsValid && sectionmodele.SEARCH == 0 && sectionmodele.ID_SECTION != sectionmodele.SEC_ID_SECTION)
            {
                if (sectionmodele.IconFile != null)
                {
                    string directoryPath = Server.MapPath("~/dist/img/icon/");
                    if (!Directory.Exists(directoryPath))
                    {
                        Directory.CreateDirectory(directoryPath);
                    }
                    string fileName = Path.GetFileName(sectionmodele.IconFile.FileName);
                    string extension = Path.GetExtension(fileName);
                    string nameWithoutExtension = Path.GetFileNameWithoutExtension(fileName);
                    string path = Path.Combine(Server.MapPath("~/dist/img/icon/"), fileName);
                    var i = 1;
                    while (System.IO.File.Exists(path))
                    {
                        fileName = nameWithoutExtension.Trim() + " (" + i + ")" + extension;
                        path = Path.Combine(Server.MapPath("~/dist/img/icon/"), fileName);
                        i++;
                    }
                    sectionmodele.IconFile.SaveAs(path);
                    sectionmodele.ICONE = fileName;
                }

                Section section = new Section
                {
                    ID_SECTION = sectionmodele.ID_SECTION,
                    SEC_ID_SECTION = sectionmodele.SEC_ID_SECTION,
                    NOM_SECTION = sectionmodele.NOM_SECTION,
                    ICONE = sectionmodele.ICONE,
                    AS_TABS = sectionmodele.AS_TABS
                };
                db.Entry(section).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            SelectList list = null;
            if (sectionmodele.SEARCH == 1)
            {
                list = new SelectList(db.Section.Where(Section => Section.SEC_ID_SECTION == sectionmodele.SEC_ID_SECTION), "ID_SECTION", "NOM_SECTION");
                ModelState.Clear();
                sectionmodele.PARENT_SEC_ID_SECTION = sectionmodele.SEC_ID_SECTION;
            }
            else if (sectionmodele.SEARCH == 0)
            {
                list = new SelectList(db.Section.Where(Section => Section.SEC_ID_SECTION == sectionmodele.PARENT_SEC_ID_SECTION), "ID_SECTION", "NOM_SECTION", sectionmodele.SEC_ID_SECTION);
            }
            if (sectionmodele.ID_SECTION == sectionmodele.SEC_ID_SECTION)
            {
                ViewBag.Message = "Une section ne peut pas être une sous section de lui même!";
            }
            ViewBag.SEC_ID_SECTION = list.Prepend(new SelectListItem { Value = null });
            return View(sectionmodele);
        }

        // POST: SectionsAdmin/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(decimal id)
        {
            if (id.Equals(null))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Section section = db.Section.Find(id);
            if (section == null)
            {
                return HttpNotFound();
            }
            db.Section.Remove(section);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
