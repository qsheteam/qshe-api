﻿using qhse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace qhse.Controllers
{
    public class HomeController : BaseController
    {
        private readonly RavinalaEntities db = new RavinalaEntities();
        public ActionResult Index()
        {
            ViewBag.stat = db.statpourcentage.Find(0);
            return View();
        }
    }
}
