﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using qhse.Models;

namespace qhse.Controllers
{
    public class GestionRubriquesController : BaseController
    {
        private readonly RavinalaEntities db = new RavinalaEntities();

        // GET: RubriquesAdmin
        public ActionResult Index()
        {
            var rubrique = db.Rubrique.Include(r => r.Section);
            return View(rubrique.ToList());
        }

        // GET: RubriquesAdmin/Create
        public ActionResult Create()
        {
            SelectList list = new SelectList(db.Section.Where(Section => Section.SEC_ID_SECTION == null), "ID_SECTION", "NOM_SECTION");
            ViewBag.ID_SECTION = list.Prepend(new SelectListItem { Value = null });
            return View();
        }

        // POST: RubriquesAdmin/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_SECTION,NOM_RUBRIQUE,SEARCH,PARENT_SEC_ID_SECTION")] RubriqueModele rubriquemodele)
        {
            if (ModelState.IsValid && rubriquemodele.SEARCH == 0)
            {
                Rubrique rubrique = new Rubrique
                {
                    ID_SECTION = rubriquemodele.ID_SECTION,
                    NOM_RUBRIQUE = rubriquemodele.NOM_RUBRIQUE
                };
                db.Rubrique.Add(rubrique);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            SelectList list = null;
            if (rubriquemodele.SEARCH == 1)
            {
                if (rubriquemodele.ID_SECTION == 0)
                {
                    list = new SelectList(db.Section.Where(Section => Section.SEC_ID_SECTION == null), "ID_SECTION", "NOM_SECTION");
                    rubriquemodele.PARENT_SEC_ID_SECTION = null;
                }
                else if (rubriquemodele.ID_SECTION != 0)
                {
                    list = new SelectList(db.Section.Where(Section => Section.SEC_ID_SECTION == rubriquemodele.ID_SECTION), "ID_SECTION", "NOM_SECTION");
                    rubriquemodele.PARENT_SEC_ID_SECTION = rubriquemodele.ID_SECTION;
                }
                ModelState.Clear();
            }
            else if (rubriquemodele.SEARCH == 0)
            {
                list = new SelectList(db.Section.Where(Section => Section.SEC_ID_SECTION == rubriquemodele.PARENT_SEC_ID_SECTION), "ID_SECTION", "NOM_SECTION", rubriquemodele.ID_SECTION);
            }
            ViewBag.ID_SECTION = list.Prepend(new SelectListItem { Value = null });
            return View(rubriquemodele);
        }

        // GET: RubriquesAdmin/Edit/5
        public ActionResult Edit(decimal id)
        {
            if (id.Equals(null))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rubrique rubrique = db.Rubrique.Find(id);
            if (rubrique == null)
            {
                return HttpNotFound();
            }
            Section parentSec = db.Section.Where(Section => Section.ID_SECTION == rubrique.ID_SECTION).FirstOrDefault();
            if (parentSec == null)
            {
                parentSec = new Section { SEC_ID_SECTION = null };
            }
            RubriqueModele rubriquemodele = new RubriqueModele
            {
                ID_RUBRIQUE = rubrique.ID_RUBRIQUE,
                ID_SECTION = rubrique.ID_SECTION,
                NOM_RUBRIQUE = rubrique.NOM_RUBRIQUE,
                PARENT_SEC_ID_SECTION = parentSec.SEC_ID_SECTION
            };
            SelectList list = new SelectList(db.Section.Where(Section => Section.SEC_ID_SECTION == rubriquemodele.PARENT_SEC_ID_SECTION), "ID_SECTION", "NOM_SECTION", rubrique.ID_SECTION);
            ViewBag.ID_SECTION = list.Prepend(new SelectListItem { Value = null });
            return View(rubriquemodele);
        }

        // POST: RubriquesAdmin/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_RUBRIQUE,ID_SECTION,NOM_RUBRIQUE,SEARCH,PARENT_SEC_ID_SECTION")] RubriqueModele rubriquemodele)
        {
            if (ModelState.IsValid && rubriquemodele.SEARCH == 0)
            {
                Rubrique rubrique = new Rubrique
                {
                    ID_RUBRIQUE = rubriquemodele.ID_RUBRIQUE,
                    ID_SECTION = rubriquemodele.ID_SECTION,
                    NOM_RUBRIQUE = rubriquemodele.NOM_RUBRIQUE
                };
                db.Entry(rubrique).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            SelectList list = null;
            if (rubriquemodele.SEARCH == 1)
            {
                if (rubriquemodele.ID_SECTION == 0)
                {
                    list = new SelectList(db.Section.Where(Section => Section.SEC_ID_SECTION == null), "ID_SECTION", "NOM_SECTION");
                    rubriquemodele.PARENT_SEC_ID_SECTION = null;
                }
                else if (rubriquemodele.ID_SECTION != 0)
                {
                    list = new SelectList(db.Section.Where(Section => Section.SEC_ID_SECTION == rubriquemodele.ID_SECTION), "ID_SECTION", "NOM_SECTION");
                    rubriquemodele.PARENT_SEC_ID_SECTION = rubriquemodele.ID_SECTION;
                }
                ModelState.Clear();
            }
            else if (rubriquemodele.SEARCH == 0)
            {
                list = new SelectList(db.Section.Where(Section => Section.SEC_ID_SECTION == rubriquemodele.PARENT_SEC_ID_SECTION), "ID_SECTION", "NOM_SECTION", rubriquemodele.ID_SECTION);
            }
            ViewBag.ID_SECTION = list.Prepend(new SelectListItem { Value = null });
            return View(rubriquemodele);
        }

        // POST: RubriquesAdmin/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(decimal id)
        {
            if (id.Equals(null))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rubrique rubrique = db.Rubrique.Find(id);
            if (rubrique == null)
            {
                return HttpNotFound();
            }
            db.Rubrique.Remove(rubrique);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
