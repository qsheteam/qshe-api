﻿using qhse.Core;
using qhse.Models;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.IO;
using System;

namespace qhse.Controllers
{
    public class ContentController : ApiController
    {
        [HttpPost]
        [Route("api/contents/jsons")]
        public ApiResponse Insert([FromUri] string jsonList)
        {
            var api = new ApiResponse();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            ContentCollection collection = serializer.Deserialize<ContentCollection>(jsonList);
            List<Content> list = new List<Content>(collection.Contents);
            var reponse = ContentService.insertionMultiple(list);
            if (reponse != 0)
            {
                api.Status = 200;
                api.Message = "insertion de la liste réussi";
                
            }
            else
            {
                api.Status = 404;
                api.Message = "echec de l'insertion";
            }
            return api;
        }
        [HttpPost]
        [Route("api/contents/json")]
        public ApiResponse Inserer([FromUri] string jsonObject)
        {
            var api = new ApiResponse();
            Content content = ContentService.ReadToObject(jsonObject);

            var reponse = ContentService.insert(content);
            if (reponse != 0)
            {
                api.Status = 200;
                api.Message = "insertion de l'objet réussi";

            }
            else
            {
                api.Status = 404;
                api.Message = "echec de l'insertion";
            }
            return api;
        }
        [HttpPost]
        [Route("api/contents")]
        public ApiResponse Insertion([FromUri] string etat, [FromUri] string proprete, [FromUri] string username,[FromUri] string idrubrique, [FromUri] string idsection,[FromUri] string comment, [FromUri] string date, [FromUri] string heure)
        {
             
            var api = new ApiResponse();
            DateTime dates = Convert.ToDateTime(date);
            DateTime heures = Convert.ToDateTime(heure);
            var user = AccountService.FindUserByUsername(username);
            Content content = new Content(user.ID_USER, ContentService.atoi(idrubrique), ContentService.atoi(idsection), comment, dates, heures, ContentService.atoi(etat) == 0, ContentService.atoi(proprete) == 0, "");

            var reponse = ContentService.insert(content);
            if (reponse != 0)
            {
                api.Status = 200;
                api.Message = "insertion de l'objet réussi";

            }
            else
            {
                api.Status = 404;
                api.Message = "echec de l'insertion";
            }
            return api;
        }
    }
}
