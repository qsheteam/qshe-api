﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using qhse.Models;

namespace qhse.Controllers
{
    public class GestionStatusController : BaseController
    {
        private readonly RavinalaEntities db = new RavinalaEntities();

        // GET: StatusAdmin
        public ActionResult Index()
        {
            return View(db.Status.ToList());
        }

        // GET: StatusAdmin/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: StatusAdmin/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "NOM_STATUS")] StatutModele statutmodele)
        {
            if (ModelState.IsValid)
            {
                Status status = new Status
                {
                    NOM_STATUS = statutmodele.NOM_STATUS
                };
                db.Status.Add(status);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(statutmodele);
        }

        // GET: StatusAdmin/Edit/5
        public ActionResult Edit(decimal id)
        {
            if (id.Equals(null))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Status status = db.Status.Find(id);
            if (status == null)
            {
                return HttpNotFound();
            }
            StatutModele statutmodele = new StatutModele
            {
                ID_STATUS = status.ID_STATUS,
                NOM_STATUS = status.NOM_STATUS
            };
            return View(statutmodele);
        }

        // POST: StatusAdmin/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_STATUS,NOM_STATUS")] StatutModele statutmodele)
        {
            if (ModelState.IsValid)
            {
                Status status = new Status
                {
                    ID_STATUS = statutmodele.ID_STATUS,
                    NOM_STATUS = statutmodele.NOM_STATUS
                };
                db.Entry(status).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(statutmodele);
        }

        // POST: StatusAdmin/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(decimal id)
        {
            if (id.Equals(null))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Status status = db.Status.Find(id);
            if (status == null)
            {
                return HttpNotFound();
            }
            db.Status.Remove(status);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
