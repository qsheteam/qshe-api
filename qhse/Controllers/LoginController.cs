﻿using qhse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace qhse.Controllers
{
    public class LoginController : Controller
    {
        private readonly RavinalaEntities RavinalaEntitie = new RavinalaEntities();
        // GET: Login
        public ActionResult Index()
        {
            ViewBag.Title = "Login";
            return View();
        }

        [HttpPost]
        public ActionResult CheckUser([Bind(Include = "pseudo,password")]Login log)
        {
            if (ModelState.IsValid)
            {
                var result = RavinalaEntitie.User.Where(user => user.PASSWORD == log.password && user.USERNAME == log.pseudo && user.Status.NOM_STATUS == "administrateur" && user.LOCK == false).FirstOrDefault();
                if (result != null)
                {
                    Session["idUser"] = result.ID_USER;
                    Session["nameUser"] = result.USERNAME;
                    Session["idStatutUser"] = result.ID_STATUS;
                    Session["nameStatutUser"] = result.Status.NOM_STATUS;
                    return RedirectToAction("Index", "Home");
                }
                else if (result == null)
                {
                    ViewBag.Message = string.Format("Pseudo ou mot de passe incorrecte!");
                }
            }
            return View("Index",log);



            
        }

        public ActionResult LougOut()
        {
            Session.Abandon();
            return RedirectToAction("Index", "Login");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                RavinalaEntitie.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}