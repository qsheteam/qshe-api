﻿using qhse.Core;
using qhse.Models;
using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace qhse.Controllers
{
    public class SectionsController : BaseController
    {
        
        // GET: Sections
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase upload)
        {
            var line = "";
            if (upload != null && upload.ContentLength > 0)
            {
                if (upload.FileName.EndsWith(".json"))
                {
                    StreamReader stream = new StreamReader(upload.InputStream);
                    using (stream)
                    {

                        line = stream.ReadToEnd();
                    }
                }
            }

            List<SectionChild> list = JsonConvert.DeserializeObject<List<SectionChild>>(line);
            SectionService.insertTree(list);
            ViewBag.Zetik = line;
            return View();
        }
    }
}