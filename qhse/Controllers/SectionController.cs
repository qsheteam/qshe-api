﻿using qhse.Core;
using qhse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace qhse.Controllers
{
    public class SectionController : ApiController
    {
        //[HttpGet]
        //[Route("api/sections")]
        //public ApiResponse getAllSection()
        //{
        //    var api = new ApiResponse();
        //    var listeSection = SectionService.getAllSection();
        //    if (listeSection.Count != 0)
        //    {
        //        api.Status = 200;
        //        api.Message = "voici la liste des sections";
        //        api.Value = listeSection;
        //    }
        //    else
        //    {
        //        api.Status = 200;
        //        api.Message = "section vide";
        //    }
        //    return api;
        //}
        //[HttpGet]
        //[Route("api/sections")]
        //public ApiResponse getChildSections([FromUri] string section)
        //{
        //    var api = new ApiResponse();
        //    var listeSection = SectionService.getSectionChild(section,SectionService.getAllSection());
        //    if (listeSection != null)
        //    {
        //        api.Status = 200;
        //        api.Message = "voici la liste des sections fils";
        //        api.Value = listeSection;
        //    }
        //    else
        //    {
        //        api.Status = 200;
        //        api.Message = "section fils vide";
        //    }
        //    return api;
        //}
        [HttpGet]
        [Route("api/sections")]
        public ApiResponse getChildSections()
        {
            var api = new ApiResponse();
            var listeSection = SectionService.listArranger(SectionService.getAllSection());
            if (listeSection != null)
            {
                api.Status = 200;
                api.Message = "voici la liste des sections fils";
                api.Value = listeSection;
            }
            else
            {
                api.Status = 200;
                api.Message = "section fils vide";
            }
            return api;
        }
    }
}
