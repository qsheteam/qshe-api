﻿using qhse.Core;
using qhse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace qhse.Controllers
{
    public class AccountController : ApiController
    {
        [HttpPost]
        [Route("api/login")]
        public ApiResponse Login([FromUri] string username, [FromUri] string password)
        {
            var response = new ApiResponse();
            var user = AccountService.Signin(username, password);
            if (user != null)
            {
                response.Status = 200;
                response.Message = "Connected!Here is your token";
                response.Value = TokenManager.GenerateToken(user);
            }
            else
            {
                response.Status = 400;
                response.Message = "Username ou mot de passe invalide";
            }
            return response;
        }
    }
}
