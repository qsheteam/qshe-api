﻿using qhse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using System.Net;

namespace qhse.Controllers
{
    public class ConsultationController : BaseController
    {
        private readonly RavinalaEntities db = new RavinalaEntities();

        // GET: Consultation
        public ActionResult Index()
        {
            var content = db.Content.Where(c => c.NOM_CONTENT != null || c.IMAGE != null).Include(c => c.Rubrique).Include(c => c.Section).Include(c => c.User);
            return View(content.ToList());
        }
    }
}