﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using qhse.Models;

namespace qhse.Controllers
{
    public class RubriquesController : ApiController
    {
        private RavinalaEntities db = new RavinalaEntities();

        // GET: api/Rubriques
        public IQueryable<Rubrique> GetRubrique()
        {
            return db.Rubrique;
        }

        public List<Rubrique> GetRubriqueBySection(int id)
        {
            return db.Rubrique.Where(r => r.ID_SECTION == id).ToList<Rubrique>();
        }

        // GET: api/Rubriques/5
        [ResponseType(typeof(Rubrique))]
        public IHttpActionResult GetRubrique(int id)
        {
            Rubrique rubrique = db.Rubrique.Find(id);
            if (rubrique == null)
            {
                return NotFound();
            }

            return Ok(rubrique);
        }

        // PUT: api/Rubriques/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutRubrique(int id, Rubrique rubrique)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != rubrique.ID_RUBRIQUE)
            {
                return BadRequest();
            }

            db.Entry(rubrique).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RubriqueExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Rubriques
        [ResponseType(typeof(Rubrique))]
        public IHttpActionResult PostRubrique(Rubrique rubrique)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Rubrique.Add(rubrique);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (RubriqueExists(rubrique.ID_RUBRIQUE))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = rubrique.ID_RUBRIQUE }, rubrique);
        }

        // DELETE: api/Rubriques/5
        [ResponseType(typeof(Rubrique))]
        public IHttpActionResult DeleteRubrique(int id)
        {
            Rubrique rubrique = db.Rubrique.Find(id);
            if (rubrique == null)
            {
                return NotFound();
            }

            db.Rubrique.Remove(rubrique);
            db.SaveChanges();

            return Ok(rubrique);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RubriqueExists(decimal id)
        {
            return db.Rubrique.Count(e => e.ID_RUBRIQUE == id) > 0;
        }
    }
}