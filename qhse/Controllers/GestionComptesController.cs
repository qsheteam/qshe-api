﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using qhse.Models;
using System.Net;


namespace qhse.Controllers
{
    public class GestionComptesController : BaseController
    {
        private readonly RavinalaEntities db = new RavinalaEntities();

        // GET: GestionCompte
        public ActionResult Index()
        {
            var user = db.User.Include(u => u.Status);
            return View(user.ToList());
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            ViewBag.ID_STATUS = new SelectList(db.Status, "ID_STATUS", "NOM_STATUS");
            return View();
        }

        // POST: Users/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_STATUS,USERNAME,PASSWORD,CPASSWORD")] UserModele usermodele)
        {
            if (ModelState.IsValid)
            {
                if (usermodele.PASSWORD != usermodele.CPASSWORD)
                {
                    ViewBag.Message = string.Format("Le mot de passe ne correspond pas au confirmation mot de passe!");
                    ViewBag.ID_STATUS = new SelectList(db.Status, "ID_STATUS", "NOM_STATUS", usermodele.ID_STATUS);
                    return View(usermodele);
                }
                User user = new User
                {
                    ID_STATUS = usermodele.ID_STATUS,
                    USERNAME = usermodele.USERNAME,
                    PASSWORD = usermodele.PASSWORD
                };
                db.User.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ID_STATUS = new SelectList(db.Status, "ID_STATUS", "NOM_STATUS", usermodele.ID_STATUS);
            return View(usermodele);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(decimal id)
        {
            if (id.Equals(null))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.User.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID_STATUS = new SelectList(db.Status, "ID_STATUS", "NOM_STATUS", user.ID_STATUS);
            UserModele usermodele = new UserModele
            {
                ID_USER = user.ID_USER,
                ID_STATUS = user.ID_STATUS,
                USERNAME = user.USERNAME,
                PASSWORD = user.PASSWORD,
                CPASSWORD = user.PASSWORD
            };
            return View(usermodele);
        }

        // POST: Users/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_USER,ID_STATUS,USERNAME,PASSWORD,CPASSWORD")] UserModele usermodele)
        {
            if (ModelState.IsValid)
            {
                if (usermodele.PASSWORD != usermodele.CPASSWORD)
                {
                    ViewBag.Message = string.Format("Le mot de passe ne correspond pas au confirmation mot de passe!");
                    ViewBag.ID_STATUS = new SelectList(db.Status, "ID_STATUS", "NOM_STATUS", usermodele.ID_STATUS);
                    return View(usermodele);
                }
                User user = new User
                {
                    ID_USER = usermodele.ID_USER,
                    ID_STATUS = usermodele.ID_STATUS,
                    USERNAME = usermodele.USERNAME,
                    PASSWORD = usermodele.PASSWORD
                };
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ID_STATUS = new SelectList(db.Status, "ID_STATUS", "NOM_STATUS", usermodele.ID_STATUS);
            return View(usermodele);
        }

        // POST: Users/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(decimal id)
        {
            if (id.Equals(null))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.User.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            db.User.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Verouiller(decimal id)
        {
            if (id.Equals(null))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.User.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            if (user.LOCK == true)
            {
                user.LOCK = false;
            }
            else if (user.LOCK == false)
            {
                user.LOCK = true;
            }
            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}