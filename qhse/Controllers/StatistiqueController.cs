﻿using qhse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace qhse.Controllers
{
    public class StatistiqueController : BaseController
    {
        private readonly RavinalaEntities db = new RavinalaEntities();
        // GET: Statistique
        public ActionResult Index()
        {
            return View(db.statpourcentage.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}