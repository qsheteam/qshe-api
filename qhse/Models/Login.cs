﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace qhse.Models
{
    public class Login
    {
        [Display(Name = "Pseudo")]
        [Required(ErrorMessage = "Veuillez entrer votre pseudo!")]
        public String pseudo { get; set; }

        [Display(Name = "Mot de passe")]
        [Required(ErrorMessage = "Veuillez entrer votre mot de passe!")]
        public String password { get; set; }
    }
}