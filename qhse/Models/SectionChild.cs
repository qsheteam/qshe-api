﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace qhse.Models
{
    public class SectionChild : Section
    {
        public List<SectionChild> Enfants { get; set; }
        public void CopyInto(Section section)
        {
            this.ID_SECTION = section.ID_SECTION;
            this.SEC_ID_SECTION = section.SEC_ID_SECTION;
            this.NOM_SECTION = section.NOM_SECTION;
            this.ICONE = section.ICONE;
            this.AS_TABS = section.AS_TABS;
        }

        public SectionChild()
        {
            this.Enfants = new List<SectionChild>();
        }
        public SectionChild(Section section)
        {
            this.ID_SECTION = section.ID_SECTION;
            this.SEC_ID_SECTION = section.SEC_ID_SECTION;
            this.NOM_SECTION = section.NOM_SECTION;
            this.ICONE = section.ICONE;
            this.Enfants = new List<SectionChild>();
        }

        internal Section ToSection()
        {
            return new Section(SEC_ID_SECTION, NOM_SECTION, ICONE, AS_TABS);
        }
    }
}