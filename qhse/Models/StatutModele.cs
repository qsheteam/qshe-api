﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace qhse.Models
{
    public class StatutModele
    {
        public StatutModele()
        {
            
        }

        public decimal ID_STATUS { get; set; }

        [Display(Name = "Statut")]
        [Required(ErrorMessage = "Veuillez entrer le nom du statut!")]
        public string NOM_STATUS { get; set; }
    }
}