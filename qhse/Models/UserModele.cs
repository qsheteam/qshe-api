﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace qhse.Models
{
    public class UserModele
    {
        public UserModele()
        {
            
        }

        public decimal ID_USER { get; set; }

        [Display(Name = "Statut")]
        [Required(ErrorMessage = "Veuillez selectioner le statut!")]
        public decimal ID_STATUS { get; set; }

        [Display(Name = "Pseudo")]
        [Required(ErrorMessage = "Veuillez entrer le pseudo!")]
        public string USERNAME { get; set; }

        [Display(Name = "Mot de passe")]
        [Required(ErrorMessage = "Veuillez entrer le mot de passe!")]
        public string PASSWORD { get; set; }

        [Display(Name = "Confirmation mot de passe")]
        [Required(ErrorMessage = "Veuillez confirmer le mot de passe!")]
        public string CPASSWORD { get; set; }
    }
}