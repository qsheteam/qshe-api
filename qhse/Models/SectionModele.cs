﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Drawing.Imaging;
using System.Drawing;
using System.IO;

namespace qhse.Models
{
    public class SectionModele
    {
        public SectionModele(){}

        public decimal ID_SECTION { get; set; }

        public Nullable<decimal> SEC_ID_SECTION { get; set; }
        public Nullable<decimal> PARENT_SEC_ID_SECTION { get; set; }

        [Display(Name = "Section")]
        [Required(ErrorMessage = "Veuillez entrer le nom de la section!")]
        public string NOM_SECTION { get; set; }

        [Display(Name = "Icon")]
        public string ICONE { get; set; }

        [Display(Name = "Onglet")]
        [Required(ErrorMessage = "Veuillez confirmer si s'est un onglet!")]
        public bool AS_TABS { get; set; }

        public int SEARCH { get; set; }

        [Display(Name = "Fichier icon")]
        [Required(ErrorMessage = "Veuillez selectionner une image!")]
        [ValidateFile(ErrorMessage = "Seul les images jpg,jpeg,ico et png sont autorisé!")]
        public HttpPostedFileBase IconFile { get; set; }
    }

    public class ValidateFileAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            HttpPostedFileBase file = value as HttpPostedFileBase;
            if (file != null)
            {
                String ext = Path.GetExtension(file.FileName);
                if (ext == ".jpg" || ext == ".jpeg" || ext == ".ico" || ext == ".png")
                {
                    return true;
                }
            }
            return false;
        }
    }
}