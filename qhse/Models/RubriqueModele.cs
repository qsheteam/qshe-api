﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace qhse.Models
{
    public class RubriqueModele
    {
        public RubriqueModele()
        {
            
        }

        public decimal ID_RUBRIQUE { get; set; }

        [Display(Name = "Section")]
        [Required(ErrorMessage = "Veuillez selectioner la section!")]
        public decimal ID_SECTION { get; set; }

        [Display(Name = "Rubrique")]
        [Required(ErrorMessage = "Veuillez entrer le nom de la rubrique!")]
        public string NOM_RUBRIQUE { get; set; }

        public int SEARCH { get; set; }

        public Nullable<decimal> PARENT_SEC_ID_SECTION { get; set; }
    }
}